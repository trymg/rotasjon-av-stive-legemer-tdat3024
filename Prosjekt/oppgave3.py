from oppgave1a import exp, lagomega, lagOmega
from oppgave1b import lagL
import numpy as np

from oppgave1c import treghetsmomentTNokkel


# Implementasjon av eulers metode på T-nøkkelen
def eulerTNokkel(X, w, start, slutt, h, returnerAlt=False):
    # Bestemmer treghetsmoment I ved å bruke treghetsmomentTNokkel-funksjonen fra 1c
    # Bestemmer dreiemoment L ved å bruke makeL-funksjonen fra 1b
    # Returnerer resultatet av euler for input-verdiene for t-nøkkel
    I = treghetsmomentTNokkel()
    L = lagL(I, w)
    return euler(X, start, slutt, h, I, L, returnerAlt)


# Implementasjon av eulers metode. Returnerer array
def euler(X, start, slutt, h, I, L, returnerAlt=False):
    # Legger først X inn i arrayet Wer
    # I_inv er den inverse av treghetsmoment-matrisen
    # Går inn i løkke hvor vi for hver t i intervallet gjør følgende:
    # Transponerer W og kaller denne W_tra
    # Setter w lik I_inv*W_tra*L, hvor L er dreiemoment
    # Sender inn w til makeOmega-funksjonen fra 1a, og kaller reultatet for Omega
    # Setter W lik W * eksponensialfunksjonen fra 1a med h og omega som inputverdier
    # Legger W i arrayet Wer
    # Etter løkka returnerer vi W
    Wer = []
    W = X
    Wer.append(W)
    I_inv = np.linalg.inv(I)
    for t in np.arange(start, slutt, h):
        W_tra = W.transpose()
        w = I_inv @ W_tra @ L
        Omega = lagOmega(w)
        W = W @ exp(h, Omega)
        Wer.append(W)

    if returnerAlt:
        return np.array(Wer)
    else:
        return np.array(W)


if __name__ == "__main__":
    I = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    X = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    L = [1, 0, 0]
    ans = euler(X, 0, 10, 0.0001, I, L)
    print("Euler på kula ved t=10 (h=0.0001):\n", ans)
