from tqdm import tqdm
from oppgave3 import eulerTNokkel
from oppgave4 import RK4, RKF45
import matplotlib.pyplot as plt
import numpy as np


# skriver enhetsvektordataen til tekstfil som kan brukes i simuleringsprogrammet
# skriver kommaseparerte verdier, først i-vektor, så j-vektor og så k-vektor (3 flyttall hver)
# dersom variableSteg=True skriver den også stegstørrelsen før de tre vektorene.
def skrivEnhetsvektorerTilFil(W, filnavn, variableSteg=False):
    f = open("data/" + filnavn + ".txt", "w")
    Xs = W
    for i in tqdm(range(len(Xs)), desc="Writing to file: " + filnavn):
        if i > 0:
            f.write(",")
        if variableSteg and (i + 1) % 2 == 0:
            stepSize = Xs[i]
            f.write(str(stepSize))
        else:
            arr = np.array(Xs[i])
            x_vec = np.array([arr[:, 0]]).transpose()
            y_vec = np.array([arr[:, 1]]).transpose()
            z_vec = np.array([arr[:, 2]]).transpose()
            f.write(str(x_vec[0, 0]) + "," + str(x_vec[1, 0]) + "," + str(x_vec[2, 0]))
            f.write("," + str(y_vec[0, 0]) + "," + str(y_vec[1, 0]) + "," + str(y_vec[2, 0]))
            f.write("," + str(z_vec[0, 0]) + "," + str(z_vec[1, 0]) + "," + str(z_vec[2, 0]))
    f.close()


# plotter 3x3-matrise-dataen som 9 grafer som viser komponentene til enhetsvektorene over tid
# variableSteg må settes til True dersom stegstørrelsen varierer for matrise-dataen
def visGraf(start, slutt, W, tittel, variableSteg=False):
    fig, axs = plt.subplots(3, 3)
    fig.suptitle(tittel)
    plt.tight_layout(h_pad=2)
    plt.subplots_adjust(top=0.85)
    if variableSteg:
        Ws = W[0:len(W):2]
        xs = W[1:len(W):2]
        y = []
        x = []
        for j in range(len(Ws)):
            y.append(Ws[j].reshape(-1))
        t = 0
        x.append(t)
        for j in range(len(xs)):
            t += xs[j]
            x.append(t)
        y = np.array(y).reshape(-1)
    else:
        y = W.reshape(-1)
        x = np.linspace(start, slutt, len(y) // 9)
    for i in range(9):
        ax = axs[int(i / 3), i % 3]
        ax.plot(x, y[i:len(y):9])
        ax.set_ylim(-1.2, 1.2)
        ax.set_title("X[%i,%i]" % (int(i / 3), i % 3))
    plt.show()


if __name__ == "__main__":
    X = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])

    w_vec_a = np.array([1, 0.05, 0])
    w_vec_b = np.array([0, 1, 0.05])
    w_vec_c = np.array([0.05, 0, 1])

    h = 0.005
    start = 0
    slutt = 100

    Wer_a_euler = eulerTNokkel(X, w_vec_a, start, slutt, h, True)
    Wer_b_euler = eulerTNokkel(X, w_vec_b, start, slutt, h, True)
    Wer_c_euler = eulerTNokkel(X, w_vec_c, start, slutt, h, True)

    Wer_a_rk4 = RK4(X, w_vec_a, start, slutt, h, True)
    Wer_b_rk4 = RK4(X, w_vec_b, start, slutt, h, True)
    Wer_c_rk4 = RK4(X, w_vec_c, start, slutt, h, True)

    tol = 1e-6
    h_start = 0.1  # Steglengden oppdateres av RKF for å holde oss innen toleransen
    Wer_a_rkf = RKF45(X, w_vec_a, start, slutt, h_start, tol, True)
    Wer_b_rkf = RKF45(X, w_vec_b, start, slutt, h_start, tol, True)
    Wer_c_rkf = RKF45(X, w_vec_c, start, slutt, h_start, tol, True)

    visGraf(start, slutt, Wer_a_euler, "Euler med w=%s og steglengde h=%s" % (w_vec_a, h))
    visGraf(start, slutt, Wer_b_euler, "Euler med w=%s og steglengde h=%s" % (w_vec_b, h))
    visGraf(start, slutt, Wer_c_euler, "Euler med w=%s og steglengde h=%s" % (w_vec_c, h))
    visGraf(start, slutt, Wer_a_rk4, "RK4 med w=%s og steglengde h=%s" % (w_vec_a, h))
    visGraf(start, slutt, Wer_b_rk4, "RK4 med w=%s og steglengde h=%s" % (w_vec_b, h))
    visGraf(start, slutt, Wer_c_rk4, "RK4 med w=%s og steglengde h=%s" % (w_vec_c, h))
    visGraf(start, slutt, Wer_a_rkf, "RKF med w=%s og toleranse tol=%s" % (w_vec_a, tol), True)
    visGraf(start, slutt, Wer_b_rkf, "RKF med w=%s og toleranse tol=%s" % (w_vec_b, tol), True)
    visGraf(start, slutt, Wer_c_rkf, "RKF med w=%s og toleranse tol=%s" % (w_vec_c, tol), True)

    skrivEnhetsvektorerTilFil(Wer_a_rk4, "Wer_a_rk4")
    skrivEnhetsvektorerTilFil(Wer_b_rk4, "Wer_b_rk4")
    skrivEnhetsvektorerTilFil(Wer_c_rk4, "Wer_c_rk4")

    skrivEnhetsvektorerTilFil(Wer_a_euler, "Wer_a_euler")
    skrivEnhetsvektorerTilFil(Wer_b_euler, "Wer_b_euler")
    skrivEnhetsvektorerTilFil(Wer_c_euler, "Wer_c_euler")

    skrivEnhetsvektorerTilFil(Wer_a_rkf, "Wer_a_rkf", True)
    skrivEnhetsvektorerTilFil(Wer_b_rkf, "Wer_b_rkf", True)
    skrivEnhetsvektorerTilFil(Wer_c_rkf, "Wer_c_rkf", True)
