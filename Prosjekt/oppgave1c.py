from oppgave1b import *

"""
bruker volum og tetthet til å regne ut massen til t-nøkkelen
deretter regnes I ved å ta summen av (m * r²)/k for hver akse
der k varierer avhengig av aksen det roteres om
til slutt legges hver akse av I sammen til èn matrise
"""


def treghetsmomentTNokkel():
    L1 = 8.e-2  # 8cm
    R1 = 1.e-2  # 1cm
    L2 = 4.e-2  # 4cm
    R2 = 1.e-2  # 1cm
    rho = 6.7e3  # 6.7g/cm^3
    masseSenterHandtakX = -1.e-2  # -1cm, massesenter håndtak x-akse
    masseSenterSylinderX = 2.e-2  # 2cm, massesenter sylinder x-akse
    M = 12e-3 * pi * rho  # 12*PI gram (=253)
    masseSenterTNøkkel = [0, 0, 0]

    V1 = sylinderVolum(R1, L1)  # volum for håndtak
    V2 = sylinderVolum(R2, L2)  # volum for det andre sylinderet

    M1 = V1 * rho  # masse for håndtak
    M2 = V2 * rho  # masse for det andre sylinderet

    # regner ut I for alle 3 akser
    Ixy = Iyx = Ixz = Izx = Iyz = Izy = 0.
    Ixx = (M1 * R1 * R1) / 4 + (M1 * L1 * L1) / 12 + (M2 * R2 * R2) / 2
    Iyy = M1 * R1 * R1 + (M2 * L2 * L2) / 4 + (M1 * R1 * R1) / 2 + (M2 * R2 * R2) / 4 + (M2 * L2 * L2) / 12
    Izz = M1 * R1 * R1 + (M2 * L2 * L2) / 4 + (M1 * R1 * R1) / 4 + (M1 * L1 * L1) / 12 + (M2 * R2 * R2) / 4 + (
            M2 * L2 * L2) / 12

    # legger sammen I til èn matrise
    I = np.array([[Ixx, Ixy, Ixz],
                  [Iyx, Iyy, Iyz],
                  [Izx, Izy, Izz]])

    return I


if __name__ == "__main__":
    # printer treghetsmoment og rotasjonsenergi
    I = treghetsmomentTNokkel()
    res = rotasjonsEnergi(I, [1, 0.05, 0])
    print('Treghetsmoment I:\n', I)
    print()
    print('Rotasjonsenergi: ', str(res) +"J")
