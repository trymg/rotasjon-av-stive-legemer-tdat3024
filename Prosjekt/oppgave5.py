import time

from oppgave1a import testX
import numpy as np
from oppgave1b import lagL, rotasjonsEnergi
from oppgave1c import treghetsmomentTNokkel
from oppgave4 import RK4, RKF45

"""
benytter tidligere løsninger til å løse systemet beskrevet i oppgave 3
bruker treghetsmoment, dreiemoment, og X_0 = identitetsmatrisa
steglengden h settes lav nok til at energien ikke endrer seg signifikant
RKF4/RKF45 kjøres med disse argumentene og vektorene w_vec fra oppgave 3
"""


def oppgave5(w_vec, start, slutt, h, brukRKF45=False, tol=1e-8):
    if brukRKF45:
        print("Oppgave 5 med RKF45, toleranse = " + str(tol) + " og w_vec = " + str(w_vec))
    else:
        print("Oppgave 5 med RK4, steglengde = " + str(h) + " og w_vec = " + str(w_vec))

    # finner argumenter som skal brukes    
    X = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])  # settes lik identitetsmatrisa
    I = treghetsmomentTNokkel()
    K = rotasjonsEnergi(I, w_vec)
    L = lagL(I, w_vec)

    print("X starter som:\n", X)
    print("w_vec starter som", w_vec, "med lengde:", np.linalg.norm(w_vec))

    print("Startenergi: %sJ" % K)

    # kjører argumentene gjennom RKF45/RKF4
    if brukRKF45:
        X_i = RKF45(X, w_vec, start, slutt, h, tol)  # X løst med RKF45
    else:
        X_i = RK4(X, w_vec, start, slutt, h)  # X løst med RK4

    print()
    print("X_i ble til:\n", X_i)

    w_vec_i = np.linalg.inv(X_i @ I) @ L  # løsning for rotasjonsfart

    print("w_vec ble til", w_vec_i, "med lengde:", np.linalg.norm(w_vec_i))

    K_i = rotasjonsEnergi(I, w_vec_i)
    print("Sluttenergi: %sJ" % K_i)

    if testX(X_i):
        print("X test suksess")
    else:
        print("X test feilet")

    print()
    print()


if __name__ == "__main__":
    np.set_printoptions(suppress=True)

    # oppgitte vektorer
    w_vec_a = np.array([1, 0.05, 0])
    w_vec_b = np.array([0, 1, 0.05])
    w_vec_c = np.array([0.05, 0, 1])

    h = 0.005
    start = 0
    slutt = 100

    # kjører løsningen med RK4
    oppgave5(w_vec_a, start, slutt, h)
    oppgave5(w_vec_b, start, slutt, h)
    oppgave5(w_vec_c, start, slutt, h)

    # kjører løsningen med RKF45
    oppgave5(w_vec_a, start, slutt, h, True)
    oppgave5(w_vec_b, start, slutt, h, True)
    oppgave5(w_vec_c, start, slutt, h, True)
