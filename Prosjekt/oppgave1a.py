from math import cos, sin
import numpy as np


# implementasjon av eksponensialfunksjonen, tar steglengde h og en 3x3-matrise som input
def exp(h, Omega):
    # lager w_mat ved å hente Wx, Wy og Wz fra Omega
    # I er identitetsmatrisa til Omega
    # w er normen til w_mat
    # returnerer matrisa X som er resultatet av eksponensialfunksjonen
    w_mat = [Omega[2][1], Omega[0][2], Omega[1][0]]
    I = np.identity(len(Omega))
    w = np.linalg.norm(w_mat)
    return I + (1 - cos(w * h)) * Omega @ Omega / (w ** 2) + sin(w * h) * Omega / w


# Testing av eksponensialfunksjonen ved å sjekke at X*X^T = I og at determinanten til X er 1
def testX(X):
    # Kaller den transponerte av X for Xt
    # I er identitetsmatrisa til X
    # Returnerer true hvis determinanten til X er 1 +- 1e-10 og X*X^T = I +- 1e-10, false hvis ikke
    Xt = X.transpose()
    I = np.identity(len(X))
    return np.isclose(np.linalg.det(X), 1, atol=1e-10) and np.isclose(Xt.dot(X), I, atol=1e-10).all()


# Sjekker om error er mindre enn 1e-15
def testExpOutput(X):
    # Kaller den transponerte av X for Xt
    # I er identitetsmatrisa til X
    # Dot er Xt*X
    # Printer dot
    # Returnerer true hvis error er mindre enn 1e-15, false hvis ikke
    Xt = X.transpose()
    I = np.identity(len(X))
    dot = Xt @ X
    print("X_t*X:\n", dot)
    return np.isclose(dot, I, atol=1e-15).all()


def lagomega(Omega):
    w_mat = [Omega[2][1], Omega[0][2], Omega[1][0]]
    return w_mat


# Lager 3x3-matrise basert på input omega
def lagOmega(omega):
    # Setter wx, wy, wz lik verdiene i omega
    # Lager 3x3-matrise Omega og setter elementene fra omega på riktig plass
    # Returnerer omega
    wx, wy, wz = omega
    Omega = np.array([[0, -wz, wy],
                      [wz, 0, -wx],
                      [-wy, wx, 0]])
    return Omega


if __name__ == "__main__":
    X = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    omega = [1, 0.05, 0]  # vinkelhastighet
    h = 1
    Omega = lagOmega(omega)

    res = exp(h, Omega)

    print("X test: ", testX(X))
    print("Resulting matrix:\n", res)
    print("Error within 1e-15:", testExpOutput(res))
