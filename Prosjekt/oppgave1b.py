from math import pi
import numpy as np

"""
sylindervolum må regnes ut for hver av de to komponentene til t-nøkkelen
bruker formelen pi * r² * l
"""


def sylinderVolum(radius, lengde):
    return pi * radius * radius * lengde


"""
dreiemomentet L regnes ved å gange sammen treghetsmoment I og rotasjonsfart w
"""


def lagL(I, w):
    # deler opp I og w i akser
    Ixx, Iyy, Izz = I[0][0], I[1][1], I[2][2]
    Ixy = Iyx = Ixz = Izx = Iyz = Izy = 0.
    wx, wy, wz = w

    # regner ut L for hver akse
    Lx = Ixx * wx - Ixy * wy - Ixz * wz
    Ly = -Iyx * wx + Iyy * wy - Iyz * wz
    Lz = -Izx * wx - Izy * wy + Izz * wz

    # legger sammen L til èn matrise
    L = np.array([Lx, Ly, Lz])
    return L


"""
bruker dreiemoment L og rotasjonsfart w
kinetisk rotasjonsenergi kan da regnes ut med formelen 1/2 * L * w
"""


def rotasjonsEnergi(I, w):
    L = lagL(I, w)

    K = 0.5 * L @ np.array(w)
    return K
