from oppgave1a import *
from oppgave1b import lagL
from oppgave1c import treghetsmomentTNokkel
import numpy as np
from oppgave3 import eulerTNokkel


def RK4(X, w, start, slutt, h, returnAll=False):
    I = treghetsmomentTNokkel()
    I_inv = np.linalg.inv(I)

    Wer, W, L = [X], X, lagL(I, w)

    steps = np.arange(start, slutt, h)

    for _ in steps:
        # Gjør stegene beskrevet i RKF45
        # se rapport/teori for nærmere beskrivelse 

        W_tra = W.transpose()
        sigma1 = I_inv @ W_tra @ L
        Sigma1 = lagOmega(sigma1)
        sigma2 = I_inv @ exp(-(h / 2), Sigma1) @ W_tra @ L
        Sigma2 = lagOmega(sigma2)
        sigma3 = I_inv @ exp(-(h / 2), Sigma2) @ W_tra @ L
        Sigma3 = lagOmega(sigma3)
        sigma4 = I_inv @ exp(-h, Sigma3) @ W_tra @ L
        Sigma4 = lagOmega(sigma4)

        W = W @ exp((h / 6.), Sigma1 + 2 * Sigma2 + 2 * Sigma3 + Sigma4)
        Wer.append(W)

    if returnAll: return np.array(Wer)
    return W


def newStegStorrelse(h, W_val, T, err):
    # Genererer ny steglengde basert på:
    # forige steglengde - h
    # matriseverdien
    # feiltoleranse
    # nåværende feil

    p = 4
    h_new = 0.8 * ((T * W_val / err) ** (1 / (p + 1))) * h
    return h_new


def kvadratiskMatriseTilVerdi(W):
    # Tar inn en kvadratisk matrise og konverterer til en verdi som kan brukes til feil-estimat
    return np.sqrt(np.trace(W.transpose() @ W))


def RKF45(X, w, start, slutt, h_initial, tol, returnAll=False):
    I = treghetsmomentTNokkel()
    I_inv = np.linalg.inv(I)

    h, T, err = h_initial, tol, 0
    Wer, W, L = [X], X, lagL(I, w)

    A = [[],
         [1. / 4],
         [3. / 32, 9. / 32],
         [1932. / 2197, -7200. / 2197, 7296. / 2197],
         [439. / 216, -8, 3680. / 513, -845. / 4104],
         [-8. / 27, 2, -3544. / 2565, 1859. / 4104, -11. / 40]]

    # Butcher-Tableu
    # -> Skjemaet som brukes i RKF45

    B = [[25. / 216, 0, 1408. / 2565, 2197. / 4104, -1. / 5],
         [16. / 135, 0, 6656. / 12825, 28561. / 56430, -9. / 50, 2. / 55]]

    t = start
    while t < slutt:
        if t > start:
            h = newStegStorrelse(h, kvadratiskMatriseTilVerdi(W), T, err)
        W_tra = W.transpose()

        relErrTest, fails, Z = False, 0, None

        while relErrTest is False:

            # Gjør stegene beskrevet i RKF45 
            # se rapport/teori for nærmere beskrivelse 

            sigma1 = I_inv @ W_tra @ L
            Sigma1 = lagOmega(sigma1)

            sigma2 = I_inv @ exp(-h, A[1][0] * Sigma1) @ W_tra @ L
            Sigma2 = lagOmega(sigma2)

            sigma3 = I_inv @ exp(-h, A[2][0] * Sigma1 + A[2][1] * Sigma2
                                 ) @ W_tra @ L
            Sigma3 = lagOmega(sigma3)

            sigma4 = I_inv @ exp(-h, A[3][0] * Sigma1 + A[3][1] * (Sigma2
                                                                   ) + A[3][2] * Sigma3) @ W_tra @ L
            Sigma4 = lagOmega(sigma4)

            sigma5 = I_inv @ exp(-h, A[4][0] * Sigma1 + A[4][1] * (Sigma2
                                                                   ) + A[4][2] * Sigma3 + A[4][3]) @ W_tra @ L
            Sigma5 = lagOmega(sigma5)

            sigma6 = I_inv @ exp(-h, A[5][0] * Sigma1 + A[5][1] * (Sigma2
                                                                   ) + A[5][2] * Sigma3 + A[5][3] * Sigma4 + A[5][
                                     4] * Sigma5) @ W_tra @ L
            Sigma6 = lagOmega(sigma6)

            W_new = W @ exp(h, B[0][0] * Sigma1 + B[0][1] * (Sigma2
                                                             ) + B[0][2] * Sigma3 + B[0][3] * Sigma4 + B[0][4] * Sigma5)

            Z = W @ exp(h, B[1][0] * Sigma1 + B[1][1] * Sigma2 + B[1][2] *
                        Sigma3 + B[1][3] * Sigma4 + B[1][4] * Sigma5 + B[1][5] * Sigma6)
            DeltaW = W_new - Z

            # Regner ut feilen

            err = kvadratiskMatriseTilVerdi(DeltaW)

            W_val = kvadratiskMatriseTilVerdi(W)
            relErrTest = err / W_val < T

            # Logisk sjekk om feilen tilfredsstiller toleransen vår
            # Dersom feilen er for stor returneres False

            if relErrTest is False:
                if fails == 0:
                    h = newStegStorrelse(h, W_val, T, err)
                elif fails > 0:
                    h = h / 2
                fails += 1

                # Bestemmer en ny h 
                # dersom det er første gang vi overskrider toleransen 
                # kaller vi en ny metode for å bestemme h
                # ellers halvverer vi h

        t += h
        W = Z
        Wer.append(h)
        Wer.append(Z)

    if returnAll: return np.array(Wer)
    return W


if __name__ == "__main__":
    w = [1, 0.05, 0]
    X = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])

    h = 0.005
    start = 0
    slutt = 10

    print("Tester fra t=%s til t=%s med steglengde h=%s" % (start, slutt, h))
    r = RK4(X, w, start, slutt, h)
    e = eulerTNokkel(X, w, start, slutt, h)
    print("Forskjell Euler og RK4:\n", (r - e))
    rkf = RKF45(X, w, start, slutt, 0.1, 1e-6)
    print("Forskjell RK4 og RKF\n", (rkf - r))
