# Rotasjon av Stive Legemer - TDAT3024

Rotasjon av Stive Legemer - Matteprosjektoppgave i TDAT3024

* Oppgave 1
    * [Oppgave 1a](/Prosjekt/oppgave1a.py)
    * [Oppgave 1b](/Prosjekt/oppgave1b.py)
    * [Oppgave 1c](/Prosjekt/oppgave1c.py)
* [Oppgave 2](/Prosjekt/oppgave2.py)
* [Oppgave 3](/Prosjekt/oppgave3.py)
* [Oppgave 4](/Prosjekt/oppgave4.py)
* [Oppgave 5](/Prosjekt/oppgave5.py)
* [Oppgave 6](/Prosjekt/oppgave6.py)


## [Rapport](https://www.overleaf.com/8279457437hpjnjrmtjgpb)