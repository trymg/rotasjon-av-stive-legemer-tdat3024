# Rapport - rotasjon av stive legemer

Rapporten skal fortelle hva dere har gjort og hvordan dere har gjort det. Den skal være bygd opp med (minst) følgende
kapitler.

* Innledning (Hvilken oppgave skal løses, og hvorfor)

I dette prosjektet skulle vi implementere matematikken bak rotasjon av en T-nøkkel i python, for så å visualisere resultatet. Prosjektet er oppdelt i seks oppgaver, hvor de fire første går ut på å lage funksjoner som kan brukes videre til å løse de to siste. Blant annet går to av oppgavene ut på å implementere forskjellige numeriske metoder som kan brukes til å løse differensiallikningene som inngår i rotasjon av en T-nøkkel. Oppgavene i prosjektet bygger altså på hverandre. 
Hensikten med prosjektet er både det å se hvordan matematikken vi har lært i TDAT3024 kan brukes i praksis, samt å bli flinkere på å implementere matematikk i kode. Det å bruke matematikken vi har lært til å simulere rotasjon av en t-nøkkel gir dypere forståelse av matematikken, blant annet ved at man enklere ser hvordan matematikken representerer virkeligheten. 
Rapporten skal dokumentere det gjennomførte prosjektet, og inneholder blant annet beskrivelser av hvordan vi løste de forskjellige oppgavene, hvordan resultatet ble, og hvorfor resultatene ble som de ble.

* Teori /Metode (Hvordan er oppgaven løst – metode og teorien bak metoden. Her er
det naturlig ˚a ta utgangspunkt i dette dokumentet og læreboka, men bruk gjerne andre
kilder ogs˚a. Men ikke skriv av/oversett det som st˚ar, studer det slik at dere forst˚ar det,
og tenk dere deretter at dere skal forklare det til en medstudent. Husk referanser – og
referanser til vitenskapelige artikler gjør seg ekstra godt.)

Oppgave 1a:
I oppgave 1 er eksponensialfunksjonen implementert og testet. Denne består av input - en 3x3-matrise, og steglengden h. Output er matrisen X, som skal tilfredsstille $X \times X^T = I$. For å teste resultatet til eksponensialfunksjonen, måtte vi ta hensyn til avrundingsfeil ved å sette toleransen i testen til absoluttverdien maskin-epsilon for python. (kondisjonstall = 1 for X?)


Oppgave 1b:

Her implementeres funksjonene for å regne ut energien til et roterende legeme. Vi bruker uttrykket $E_k = \frac{1}{2} \times r \times \vec{\omega}$. Denne bruker inputvektor, treghetsmoment I og rotasjonsvektor $\omega$

Her skal vi lage en funksjon som regner ut energien til et roterende legeme. Funksjonen skal ha inputvektor treghetsmoment Iog rotasjonsvektor 

* Resultater (Gjerne inkludert tabeller og/eller figurer)

(må fikse print statements)

* Diskusjon/Konklusjon (Hvorfor blir resultatene slik som de blir? Hva ville dere forventet ut fra teorien, og hva skjer i praksis? Hva har dere lært? Har dere forslag til ting
som kunne vært undersøkt videre og/eller ting som kunne vært gjort annerledes/bedre?)



* Referanseliste (Fullstendig URI (URL og/eller bok/artikkel-henvisning) til de og bare
de referansene dere har nevnt tidligere i rapporten.)



* Vedlegg (Utskrift av alle programmer dere har laget)

se ./utskrift

* Erklæring Hver student skal levere en erklæreing p˚a at vedkommende har samarbeidet
og bidratt konstruktivt i besvarelsen. Denne skal underskrives av de andre p˚a gruppen.



# Oppgave 2

$$I=X(t)=\text{Identitetsmatrisa}, \vec L=(1,0,0)$$

$$(3)\vec\omega = (X I)^{-1}\vec L$$

$$(3)\vec\omega = (X I)^{-1}\vec L$$

## (3)
$$\vec \omega = (XI)^{-1}\vec L = ( 
\begin{bmatrix}
1 & 0 & 0 \\
0 & 1 & 0 \\
0 & 0 & 1
\end{bmatrix}
\begin{bmatrix}
1 & 0 & 0 \\
0 & 1 & 0 \\
0 & 0 & 1
\end{bmatrix}
)^{-1}\begin{bmatrix} 1 \\ 0 \\ 0 \end{bmatrix}
$$

$$
=(\begin{bmatrix}
1 & 0 & 0 \\
0 & 1 & 0 \\
0 & 0 & 1
\end{bmatrix}
)^{-1}\begin{bmatrix} 1 \\ 0 \\ 0 \end{bmatrix}=
\begin{bmatrix}
1 & 0 & 0 \\
0 & 1 & 0 \\
0 & 0 & 1
\end{bmatrix}
\begin{bmatrix} 1 \\ 0 \\ 0 \end{bmatrix}=
\begin{bmatrix} 1 \\ 0 \\ 0 \end{bmatrix}
$$

## (4)
$$\vec\omega=(1,0,0)$$

$$
\Omega=\begin{bmatrix}
0 & -\omega_z & \omega_y \\
\omega_z & 0 & -\omega_x \\
-\omega_y & \omega_x & 0
\end{bmatrix}=
\begin{bmatrix}
0 & 0 & 0 \\
0 & 0 & -1 \\
0 & 1 & 0
\end{bmatrix}
$$
$$\frac{dX}{dt}=F(X)=X\Omega$$

$$=
\begin{bmatrix}
1 & 0 & 0 \\
0 & 1 & 0 \\
0 & 0 & 1
\end{bmatrix}
\begin{bmatrix}
0 & 0 & 0 \\
0 & 0 & -1 \\
0 & 1 & 0
\end{bmatrix}=
\begin{bmatrix}
0 & 0 & 0 \\
0 & 0 & -1 \\
0 & 1 & 0
\end{bmatrix}
$$
